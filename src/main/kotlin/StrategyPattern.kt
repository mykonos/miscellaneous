import java.lang.RuntimeException

typealias Bellverhalten = Hund.() -> Unit
typealias Laufverhalten = Hund.() -> Unit
typealias HundeName = String
typealias GewichtInKilo = Double

abstract class Hund {
    abstract val name: HundeName
    protected abstract val bellVerhalten: Bellverhalten?
    protected abstract val laufVerhalten: Laufverhalten?

    fun bellen() {
        bellVerhalten?.invoke(this) ?: throw RuntimeException("Method 'bellen' not defined for $name")
    }
    fun laufen() {
        laufVerhalten?.invoke(this)?: throw RuntimeException("Method 'laufen' not defined for $name")
    }
}

class Pudel(override val name: HundeName,
            override val bellVerhalten: Bellverhalten,
            override val laufVerhalten: Laufverhalten) : Hund()

class Dackel(override val name: HundeName,
             override val bellVerhalten: Bellverhalten,
             override val laufVerhalten: Laufverhalten,
             val gewicht: GewichtInKilo) : Hund()

class Attrappe(override val name: HundeName,
               override val bellVerhalten: Bellverhalten? = null,
               override val laufVerhalten: Laufverhalten? = null) : Hund()

val normalLaufen: Laufverhalten = { println("Normal laufen, ich heiße übrigens $name")}
val langsamLaufen: Laufverhalten = { println("Langsam laufen") }
val lautBellen: Bellverhalten = { println("Wuff, Wuff, Wuff")}
val leiseBellen: Bellverhalten = { println("wuf")}
val dackelLaufen: Bellverhalten = { println("Ich als Dackel laufe mit ${(this as Dackel).gewicht} Kilo")}

fun main() {
    val pudel1 = Pudel("Sammy", lautBellen, normalLaufen)
    val pudel2 = Pudel("Reik", leiseBellen, normalLaufen)
    val dackel1 = Dackel("Eddy", leiseBellen, dackelLaufen, 4.2)
    val dackel2 = Dackel("Eddy", lautBellen, langsamLaufen, 6.8)
    val attrappe = Attrappe("Phantom")

    pudel1.laufen()
    pudel2.bellen()
    dackel1.laufen()
    dackel2.bellen()
    attrappe.bellen()
}