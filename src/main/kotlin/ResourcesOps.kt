fun main() {
    val resName = "myResource.txt"
    val content = object{}.javaClass.getResource(resName).readText(Charsets.UTF_8)
    val lines  = object{}.javaClass.getResourceAsStream(resName).bufferedReader().readLines()
}