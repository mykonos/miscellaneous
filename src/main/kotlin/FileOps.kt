import java.io.File
import kotlin.text.Charsets.UTF_8

fun main() {
    // Reading...
    val file = File("src/main/resources/myResource.txt")
    // Is the file automatically closed after these operations?
    file.forEachLine { println(it) }
    file.forEachLine(UTF_8) { println(it) }
    file.useLines { lines -> println(lines) }
    // or do you have it to do explicitly:
    file.bufferedReader(UTF_8).use { it.readText() }
    val lines1 = file.readLines()
    val lines2 = file.bufferedReader().readLines()
    val text1 = file.readText(UTF_8)
    val bytes = file.readBytes()
    val text2 = file.inputStream().readBytes().toString(UTF_8)

    // Writing...
    file.writeText("Hello world!")
}